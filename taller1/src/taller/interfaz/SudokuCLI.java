package taller.interfaz;

/*
 * SudokuCLI.java
 * This file is part of SudokuCLI
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * SudokuCLI is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * SudokuCLI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SudokuCLI. If not, see <http://www.gnu.org/licenses/>.
 */


import taller.mundo.Sudoku;
import static taller.mundo.Sudoku.Rating;

import java.util.Map;
import java.util.Set;
import java.util.Scanner;
import java.util.ArrayList;

/**
 *  La clase <tt>SudokuCLI</tt> establece un sistema de interacción directa
 *  con un usuario final, basada en el uso de un entorno de comando de línea (CLI).
 *  A través de este, un usuario puede acceder y jugar al Sudoku.
 *  @author ISIS1206 Team
 */

public class SudokuCLI
{
	/**
	 * Referencia directa a un juego de Sudoku.
	 **/
	private Sudoku sudoku;

	/**
	 * Interfaz principal al flujo estándar de entrada.
	 * Permite obtener la entrada del usuario a través de consola
	 **/
	private Scanner in;

	/**
	 * Constuctor principal de la clase
	 **/
	public SudokuCLI()
	{
		in = new Scanner(System.in);
		sudoku = new Sudoku();
	}

	/**
	 * Menú principal del juego
	 **/
	public void mainMenu()
	{
		boolean finish = false;
		while(!finish)
		{
			Screen.clear();
			System.out.println("----------------");
			System.out.println("-              -");
			System.out.println("-    Sudoku    -");
			System.out.println("-              -");
			System.out.println("----------------\n");

			System.out.println("Menú principal");
			System.out.println("--------------");
			System.out.println("1. Iniciar nuevo juego");
			System.out.println("2. Salir\n");

			System.out.print("Seleccione una opción: ");
			int opt = in.nextInt();

			switch(opt)
			{
			case 1:
				newGame();
				break;
			case 2:
				finish = true;
				break;
			}
			Screen.clear();
		}
	}

	/**
	 * Menú de selección de dificultad de una nueva partida de Sudoku
	 **/
	public void newGame()
	{
		sudoku.resetGame();
		in.nextLine();
		Screen.clear();
		System.out.println("Nivel de dificultad");
		System.out.println("-------------------\n");

		Rating[] difficultyLvl = Rating.values();

		/**
		 * TODO: Realice un menú de selección de dificultad, de acuerdo a los niveles
		 *       declarados y dispuestos en el arreglo difficultyLvl. Nota: Es posible
		 *       realizar la impresión de cada uno de los niveles, a partir de la invocación
		 *       de la función toString(). Adicionalmente, es necesario presentar una opción 
		 *       que permita al usuario retornar al menú principal. Para este fin, es suficiente
		 *       permitir que el método actual finalice sin ejecutar instrucciones adicionales
		 **/

		for (int i = 0; i < difficultyLvl.length ; i++)
		{
			System.out.println("nivel de dificultad " + (i+1) + ": " + difficultyLvl[i].toString() );
		}


		System.out.print("Seleccione una opción entre 1 y 5: ... ");
		Scanner sc = new Scanner(System.in);

		int rta = Integer.parseInt(sc.nextLine());

		if(rta <=4)
		{
			sudoku.setUpGame(difficultyLvl[rta-1]);
			startGame(); 
		}

		else
			newGame();

	}

	/**
	 * Ciclo principal de ejecución del juego de Sudoku
	 **/


	public void startGame()
	{

		while(!sudoku.hasGameFinished())
		{
			Screen.clear();
			System.out.println(printBoard(sudoku.getBoard()));

			System.out.println("1. Ingresar un valor o Eliminar un valor");
			System.out.println("2. Eliminar un valor");
			System.out.println("3. Darse por vencido y ver la solucion del Sudoku");
			System.out.print("Seleccione una opción: ...");


			int rta = in.nextInt();

			switch(rta)
			{
			case 1:
				ingresar();
			case 2:
				eliminar();
//			case 3:
//				rendirse();
			}
		}
		//    		 
		//    		 System.out.println("2. Darse por vencido y ver la solucion del Sudoku");
		//    		 
		//    		 int op = in.nextInt();
		//    		 switch (op)
		//    		 {
		//    		 // Introducir los valores al sudoku
		//			case 1:
		//				System.out.println("Ingrese el numero de la fila a seleccionar");
		//				int a = in.nextInt();
		//				System.out.println("Ingrese el numero de la columna a seleccionar");
		//				int b = in.nextInt();
		//				System.out.println("Ingrese el numero que desea insertar la casilla");
		//				int c = in.nextInt();
		//				System.out.println("¿Desea eliminar el elemento en la casilla seleccionada? (Use true para eliminarlo / Use false para conservarlo)");
		//				boolean d = in.nextBoolean();
		//				Screen.clear();
		//				sudoku.replaceValue(a, b, c, d);
		//				System.out.println(printBoard(sudoku.getBoard()));			
		//				break;
		//				
		//			// Ver la solucion del sudoku
		//			case 2:
		//				Screen.clear();
		//				System.out.println("Solución posible al juego");
		//	             System.out.println("-------------------------");
		//	             System.out.println(printBoard(sudoku.getSolution()));
		//	             System.out.print("Presione cualquier tecla para continuar...");
		//	             in.nextLine(); 
		//	             if(in.nextLine() !=null)
		//	             {
		//	            	 Screen.clear();
		//	            	 mainMenu();
		//	             }
		//				break;
		//			}
		//    		 
		//          in.nextLine();
		//          
		//          
		//    	 }
	}

	private void ingresar() 
	{
		System.out.println("Ingrese el valor de la columna y la fila, separados cada uno por comas. Formato [f,c] [ejemplo:  1,2] : ....");
		String rta = in.nextLine();
		String[] posicion = rta.split(",");
		
		
		System.out.println("ingrese el número que desea agregar" );
		int num = in.nextInt();
		sudoku.replaceValue(Integer.parseInt(posicion[0]), Integer.parseInt(posicion[1]), num , false);
//		sudoku.replaceValue(Integer.parseInt(posicion[0]), Integer.parseInt(posicion[1]), 1, true);
	}
	
	private void eliminar() 
	{
		System.out.println("Ingrese el valor de la columna y la fila, separados cada uno por comas. Formato [f,c] [ejemplo:  1,2] : ....");
		
		String[] posicion = in.nextLine().split(",");
		
		sudoku.replaceValue(Integer.parseInt(posicion[0]), Integer.parseInt(posicion[1]), 1, true);
	}

	/**
	 * Dialogo dispuesto para la sustitución de un valor en el tablero de sudoku.
	 * Si el usuario ingresa un valor incorrecto, o si el valor ingresado viola alguna
	 * de las restricciones del juego, se informará al mismo a través de consola.
	 **/

	/**
	 * Dialogo dispuesto para la sustitución de un valor en el tablero de sudoku.
	 * Si el usuario ingresa un valor incorrecto, o si el valor ingresado viola alguna
	 * de las restricciones del juego, se informará al mismo a través de consola.
	 **/
	public void enterValue()
	{
		in.nextLine();
		System.out.println("\nIntroduzca la coordenada de la casilla, acompañada del número a introducir, e.g., A2-3\n");
		System.out.print("Casilla: ");

		String coor = in.nextLine();
		String[] values = coor.split("-");
		int num = Integer.parseInt(values[1]);
		int row = ((int) values[0].charAt(0)) - 65;
		int col = Integer.parseInt(""+values[0].charAt(1))-1;
		if(row < 0 || row > 8)
		{
			System.out.println("La coordenada de la fila debe encontrarse en el rango A-I.");
		}
		else
		{
			if(col < 0 || col > 8)
			{
				System.out.println("La coordenada de la columna debe encontrarse en el rango 1-9.");
			}
			else
			{
				String result = sudoku.replaceValue(row, col, num, false);
				System.out.println("\n"+result);
			}
		}
		System.out.print("Presione cualquier tecla para continuar...");
		in.nextLine();
	}

	/**
	 * Dialogo dispuesto para la eliminación de un valor en el tablero de sudoku.
	 * Si el usuario ingresa un valor incorrecto, o si el valor ingresado viola alguna
	 * de las restricciones del juego, se informará al mismo a través de consola.
	 **/
	public void deleteValue()
	{
		in.nextLine();
		System.out.println("\nIntroduzca la coordenada de la casilla que se desea eliminar, e.g., B4\n");
		System.out.print("Casilla: ");

		String coor = in.nextLine();
		int row = ((int) coor.charAt(0)) - 65;
		int col = Integer.parseInt(""+coor.charAt(1))-1;
		if(row < 0 || row > 8)
		{
			System.out.println("La coordenada de la fila debe encontrarse en el rango A-I.");
		}
		else
		{
			if(col < 0 || col > 8)
			{
				System.out.println("La coordenada de la columna debe encontrarse en el rango 1-9.");
			}
			else
			{
				String result = sudoku.replaceValue(row, col, 0, true);
				System.out.println("\n"+result);
			}
		}
		System.out.print("Presione cualquier tecla para continuar...");
		in.nextLine();
	}

	/**
	 * Imprime un tablero de Sudoku en consola, a partir del uso de caractéres de construcción de tablas.
	 * @param Matriz que describe un tablero de un juego de Sudoku.
	 * Cada entrada de la matriz, contiene un número entre 1 y 9 si ésta no se
	 * encuentra vacía. 0 de lo contrario
	 * @return Representación textual de un tablero de sudoku.
	 **/
	public String printBoard(int[][] board)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("\n     1   2   3   4   5   6   7   8   9\n");
		sb.append("   ┌───┬───┬───┬───┬───┬───┬───┬───┬───┐\n");
		int rowNum = 1;
		for(int[] row : board)
		{
			sb.append(" "+((char)(rowNum + 64))+" ");
			for(int col : row)
			{
				String val = (col == 0) ? " " : ""+col;
				sb.append("│ "+val+" ");
			}
			sb.append("│\n");
			if(rowNum != 9)
			{
				sb.append("   ├───┼───┼───┼───┼───┼───┼───┼───┼───┤\n");
			}
			else
			{
				sb.append("   └───┴───┴───┴───┴───┴───┴───┴───┴───┘\n");
			}
			rowNum++;
		}
		return sb.toString();
	}


}

